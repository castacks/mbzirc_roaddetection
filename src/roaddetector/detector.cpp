#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/video.hpp>
#include <string>
#include <fstream>
#include <mbzirc_commons/commons.h>
#include <options.h>

namespace mbzirc 
{
namespace roaddetection
{
  
class Detector
{
private:
    
    cv::RotatedRect target_rect_;
    cv::Mat prev_frame_;
    //cv::Mat prev_2frame_;
    bool is_first_frame_;

    std::string pathToTraining;
    std::string pathToPoly;
    cv::Mat trainingImg;
    cv::Mat testImg1;
    cv::Mat testImg2;
    cv::Mat rectangle;
    cv::Mat Poly;
    cv::Mat Pos;
    cv::Mat T;
    cv::Mat testImg1Recover;
    cv::Mat testImg2Recover;
    cv::Mat Tdiff;
    cv::Mat dst;
    cv::Mat polygonMat;

    int rect[4];
    int positions[6][2];
    int polygon[360][480];
    cv::Rect subindex;
       
public:
  
    Detector()
    {
        is_first_frame_ = true;
        readPoints(rect,positions);
        pathToTraining = mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/67.jpg";
        //pathToPoly = mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/polyImg.jpg";
        trainingImg=cv::imread(pathToTraining);
        //polygonMat=cv::imread(pathToPoly);

//trainingImg.convertTo(trainingImg,CV_8U,255.0);

        cv::imshow("trainingImg",trainingImg);
        //cv::imshow("polygonMat",polygonMat);
        ROS_INFO_STREAM("got here");
        std::cin.get();
        
        //cv::imshow("poly",polygonMat);
        //pathToTraining ="~/67.jpg";
        
        //cv::normalize(trainingImg, trainingImg, 0, 1, cv::NORM_MINMAX, CV_8UC1);
        //trainingImg.convertTo(trainingImg,CV_8U);
        //trainingImg.convertTo(trainingImg,CV_8U,255.0/(Max-Min),-255.0*Min/(Max-Min));
        //cv::normalize(trainingImg, dst, 0, 1, cv::NORM_MINMAX);
        //ROS_INFO_STREAM(pathToTraining<<" "<<trainingImg.data);
        //cv::namedWindow( "detector window test", cv::WINDOW_AUTOSIZE );
        //cv::imshow("trainingImg",dst);
        //std::cin.get();
        subindex.x=rect[0];
        subindex.y=rect[1];
        subindex.width=rect[2]-rect[0];
        subindex.height=rect[3]-rect[1];
        T=trainingImg(subindex);
        cv::cvtColor(T,T,CV_RGB2GRAY);
        cv::threshold(T, T, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
        //cv::imwrite( mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/68.jpg", T );
        //ROS_INFO_STREAM("here1"<<subindex.x<<" "<<subindex.y<<" "<<subindex.width<<" "<<subindex.height<<" "<<trainingImg.cols<<" "<<trainingImg.rows);
        
        //ROS_INFO_STREAM("here2");
        //cv::imshow("T",T);
       //std::cin.get();

        
    }
    
    bool DetectRoad(cv::Mat img_frame, cv::RotatedRect &out_target_rect)
    {
        ROS_INFO_STREAM("AAA");
        LOG_INFO("Starting road detection...");
        try
        {
            // Return if it is the first frame
            if (is_first_frame_ == true)
            {
                // Save the frame
                img_frame.copyTo(prev_frame_);
                
                is_first_frame_ = false;
                return false;
            }
            
            // Find the target
            bool isTracked = false;
            
            cv::Mat R = cv::estimateRigidTransform(img_frame,trainingImg,  false);

            if(R.cols == 0) return false;

            ROS_INFO_STREAM("BBB");

            cv::Mat H = cv::Mat(3,3,R.type());
            H.at<double>(0,0) = R.at<double>(0,0);
            H.at<double>(0,1) = R.at<double>(0,1);
            H.at<double>(0,2) = R.at<double>(0,2);

            H.at<double>(1,0) = R.at<double>(1,0);
            H.at<double>(1,1) = R.at<double>(1,1);
            H.at<double>(1,2) = R.at<double>(1,2);

            H.at<double>(2,0) = 0.0;
            H.at<double>(2,1) = 0.0;
            H.at<double>(2,2) = 1.0;

            cv::Mat Trecover;
            cv::warpPerspective(img_frame, Trecover, H, img_frame.size());
            
            // select sub image to reduce computation time
            
            Trecover=Trecover(subindex);

            //namedWindow( "Display window", WINDOW_AUTOSIZE );// Create a window for display.
            //imshow( "Display window", warped );                   // Show our image inside it.
            
            //cv::waitKey(0);
            //cv::destroyAllWindows();
            cv::imshow("image2",Trecover);
            cv::cvtColor(Trecover,Trecover,CV_RGB2GRAY);
            cv::threshold(Trecover, Trecover, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
            //cv::erode(Trecover, Trecover, cv::Mat(), cv::Point(-1, -1), 2, 1, 1);
            //cv::dilate(Trecover, Trecover, cv::Mat(), cv::Point(-1, -1), 2, 1, 1);

            //Mat img_bw = im_gray > 128;
            //cv::threshold(Trecover,Trecover,0,255,0);
            
            cv::imshow("image",Trecover);

            cv::absdiff(T, Trecover, Tdiff);
            cv::imshow("diff",Tdiff);
            ROS_INFO_STREAM("before and");
           
            cv::bitwise_and(Tdiff, polygonMat, Tdiff);

            //cv::threshold(Tdiff, Tdiff, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
            //cv::threshold(Tdiff, Tdiff, 200, 255, CV_THRESH_BINARY);
            cv::erode(Tdiff, Tdiff, cv::Mat(), cv::Point(-1, -1), 2, 1, 1);
            cv::dilate(Tdiff, Tdiff, cv::Mat(), cv::Point(-1, -1), 2, 1, 1);
            
            cv::imshow("image diff",Tdiff);
             std::cin.get();
/*          
            cv::Mat src=Tdiff;
            cv::Mat gray=Tdiff;
            int largest_area=0;
            int largest_contour_index=0;
            cv::Rect bounding_rect;
            std::vector<std::vector<cv::Point> > contours; // Vector for storing contour
            std::vector<cv::Vec4i> hierarchy;
            findContours( gray, contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
            // iterate through each contour.
            for( int i = 0; i< contours.size(); i++ )
            {
                //  Find the area of contour
                double a=contourArea( contours[i],false); 
                if(a>largest_area){
                    largest_area=a;
                    std::cout<<i<<" area  "<<a<<std::endl;
                    // Store the index of largest contour
                    largest_contour_index=i;               
                    // Find the bounding rectangle for biggest contour
                    bounding_rect=boundingRect(contours[i]);
                }
            }
            cv::Scalar color( 255,0,0);  // color of the contour in the

            /// Get the moments
            std::vector<cv::Moments> mu(contours.size() );
            for( int i = 0; i < contours.size(); i++ )
             { mu[i] = cv::moments( contours[i], false ); }

            ///  Get the mass centers:
            std::vector<cv::Point2f> mc( contours.size() );
            for( int i = 0; i < contours.size(); i++ )
             { mc[i] = cv::Point2f( mu[i].m10/mu[i].m00 , mu[i].m01/mu[i].m00 );
             std::cout<<"x: "<<mc[i].x<<" y: "<<mc[i].x<<std::endl;
              } 


            //Draw the contour and rectangle
            cv::drawContours( src, contours,largest_contour_index, color, CV_FILLED,8,hierarchy);
            cv::rectangle(src, bounding_rect,  cv::Scalar(0,255,0),2, 8,0);
            cv::namedWindow( "Display window", CV_WINDOW_AUTOSIZE );
            cv::imshow( "Display window", src ); 
            //cv::circle( src, mc );


/*          
            cv::Mat box = Tdiff;
            cv::Mat difference;
            cv::threshold(box, difference, 200, 255, CV_8UC1);
            float sumx=0, sumy=0;
            float num_pixel = 0;
            for(int x=0; x<difference.cols; x++) {
                for(int y=0; y<difference.rows; y++) {
                    int val = difference.at<uchar>(y,x);
                    if( val >= 50) {
                        sumx += x;
                        sumy += y;
                        num_pixel++;
                    }
                }
            }
            cv::Point p(sumx/num_pixel, sumy/num_pixel);
            std::cout << cv::Mat(p) << std::endl;

            cv::Moments m = cv::moments(difference, false);
            cv::Point p1(m.m10/m.m00, m.m01/m.m00);
            std::cout << cv::Mat(p1) << std::endl;

            cv::circle(difference, p, 5, cv::Scalar(128,0,0), -1);
            cv::imshow("difff", difference);
            */
             
          
            if (isTracked)
                out_target_rect = target_rect_;

            // Save the frame
            //img_frame.copyTo(prev_frame_);

            LOG_INFO("Finished road detection...");
            return isTracked;
        }
        catch (int e)
        {
            LOG_ERROR("Error in road detection...");
            ROS_ERROR("Detector exception #%i", e);
            return false;
        }
    }

    void readPoints(int *rect, int positions[6][2]){
    
    std::string path=mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/rect.txt";
    std::ifstream inputFile(path.c_str());
    if (inputFile.good()){
        int i=0;
        while(inputFile>>rect[i++]); //Check for end-of-file character
    }

    path=mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/positions.txt";
    std::ifstream inputFile2(path.c_str());
    if (inputFile2.good()){
        for(int i=0;i<6;i++){
            for(int j=0;j<2;j++){
                inputFile2>>positions[i][j];
            }
        }
    }
    
    /*
    path=mbzirc::commons::System::GetHomeDir()+"/mbzirc_ws/src/mbzirc_roaddetection/data/polygon.txt";
    std::ifstream inputFile3(path.c_str());
    if (inputFile3.good()){
        for(int i=0;i<360;i++){
            for(int j=0;j<480;j++){
                inputFile3>>polygon[i][j];
            }
        }
    */
    }

    }; // close readPoints


} // end namespace roaddetection

} // end namespace mbzirc
