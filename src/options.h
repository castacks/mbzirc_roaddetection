#ifndef __ROADDETECTION_OPTIONS_INCLUDED__
#define __ROADDETECTION_OPTIONS_INCLUDED__ 

//#define SHOWDETECTIONS		// Show all detected candidate targets on the frame
//#define SHOWEDGEIMAGE			// Show edge image
//#define SAVEIMAGES			// Save all processed frames into numbered files
//#define SAVEONLYDETECTEDIMAGES	// Save only frames where the target is found into numbered files
//#define SHOWPROCESSINGTIME		// Show time for processing each frame and the frequency of program output
//#define DRAWDETECTION			// Draw the detection on the frame image

# endif // __ROADDETECTION_OPTIONS_INCLUDED__

