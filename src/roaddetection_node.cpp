#include <ros/ros.h>
#include <image_transport/image_transport.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv_apps/RotatedRectStamped.h>
#include <mbzirc_commons/commons.h>
#include <options.h>
#include <fstream>
#include <string>
#include <geometry_msgs/Point.h>
#include <opencv_apps/RotatedRect.h>
#include <opencv_apps/Line.h>
#include <std_msgs/Int8.h>
#include <mbzirc_roaddetection/GetDir.h>
#include "mbzirc_mission/mission_modes.h"

ros::Publisher target_pub_;

bool timeStart=false;
ros::Time previousTime;
std::vector<cv::Point2f> pointsCenter;
cv::Point2f currentPoint;
cv::Point2f previousPoint;
// cv::Vec4f lineOutput;
int counter=0;
float averageDeltaX=0;
float averageDeltaY=0;
mbzirc_roaddetection::GetDir outputMsg;
int mode_;

int minCounter;
int maxSeconds;

bool finished = false;
ros::Time initialTime;
bool finishedCycle=false;

float deltaX;
float deltaY;
int countXPos=0;
int countXNeg=0;
int countYPos=0;
int countYNeg=0;
int minDeckSize;
int maxDeckSize;

void targetCallback(const opencv_apps::RotatedRectStamped::ConstPtr &msg){
  opencv_apps::RotatedRectStamped curr_rect;
  curr_rect=(*msg);
  ROS_WARN("inside roaddetec before any filter seq=%d",curr_rect.header.seq);
  // ROS_INFO_STREAM("entered targetCallback");
  //check size constraint
  //if (1) // for debugging
  if ((mode_ & WATCHPOS) && curr_rect.rect.size.width > minDeckSize && curr_rect.rect.size.width < maxDeckSize && curr_rect.rect.size.height> minDeckSize && curr_rect.rect.size.height < maxDeckSize)
        
        { 
            counter++;
        ROS_WARN("inside roaddetec after filter seq=%d",curr_rect.header.seq);
        if (counter==1) // first time begins counting
        {
           ROS_WARN("First point");
            previousTime=ros::Time::now();
          previousPoint.x=curr_rect.rect.center.x;
          previousPoint.y=curr_rect.rect.center.y;
          //ROS_INFO_STREAM("first time");
        }
        else //if the counter was not zeroed
        {
          ros::Time time_got_rect = ros::Time::now();
          if ((time_got_rect.sec-previousTime.sec)>maxSeconds) //check if too much time has passed
          {
            counter=1;
           ROS_WARN("First again");
          previousPoint.x=curr_rect.rect.center.x;
          previousPoint.y=curr_rect.rect.center.y;
            previousTime=ros::Time::now();
            // averageDeltaX=0;
            // averageDeltaY=0;
            countXPos=0;
            countXNeg=0;
            countYPos=0;
            countYNeg=0;            
            finishedCycle=false;
            //ROS_INFO_STREAM("reset counter");
          }
          else if (counter>minCounter) // check if it has already outputted a nextPos within this cycle
          {
            // do nothing, already outputted
          } else if (counter==minCounter && !finishedCycle) // output message, has reached minCounter
          {
             currentPoint.x=curr_rect.rect.center.x;
             currentPoint.y=curr_rect.rect.center.y;
             deltaX=currentPoint.x-previousPoint.x;
             deltaY=currentPoint.y-previousPoint.y;
             if (deltaX>=0){countXPos++;} else {countXNeg++;}
             if (deltaY>=0){countYPos++;} else {countYNeg++;}

             if (countXPos>countXNeg)
            {
               if (countYPos>countYNeg){outputMsg.nextPos=2;} //car going to pos 2 first
               else{outputMsg.nextPos=3;} //car going to pos 3 first
            }
            else
            {
               if (countYPos>countYNeg){outputMsg.nextPos=1;} //car going to pos 1 first
               else {outputMsg.nextPos=4;} //car going to pos 4 first
            }
            target_pub_.publish(outputMsg);
            finishedCycle=true;
            // ROS_INFO_STREAM("counter_value "<<counter);
          } 
          else //keep adding points
          {
             currentPoint.x=curr_rect.rect.center.x;
             currentPoint.y=curr_rect.rect.center.y;
             // averageDeltaX=averageDeltaX*counter/(counter+1)+(currentPoint.x-previousPoint.x)/(counter+1);
             // averageDeltaY=averageDeltaY*counter/(counter+1)+(currentPoint.y-previousPoint.y)/(counter+1);
             deltaX=currentPoint.x-previousPoint.x;
             deltaY=currentPoint.y-previousPoint.y;
             if (deltaX>=0){countXPos++;} else {countXNeg++;}
             if (deltaY>=0){countYPos++;} else {countYNeg++;}

             // ROS_INFO_STREAM("added point "<<curr_rect.rect.center.x<<" "<<curr_rect.rect.center.y);
             // ROS_INFO_STREAM("Ax "<<averageDeltaX<<" Ay"<<averageDeltaY);
             // ROS_INFO_STREAM(countXPos<<" "<<countXNeg<<" "<<countYPos<<" "<<countYNeg);
          }
        }

      }

}



int main(int argc, char** argv)
{
    // Initialize the node
    ros::init(argc, argv, "road_detection");
    ros::NodeHandle nh;
    
    outputMsg.speed=0;
    outputMsg.angle=0;
    
    ros::Rate loop_rate(30);
    
    nh.param("/road_detection/minCounter", minCounter, 4);
    nh.param("/road_detection/maxSeconds", maxSeconds, 2);
    nh.param("/road_detection/minDeckSize", minDeckSize, 50);
    nh.param("/road_detection/maxDeckSize", maxDeckSize, 130);

        
    // Initialize publisher and subscriber
    ros::Subscriber image_sub_ = nh.subscribe("/deck_track/target", 1, &targetCallback);
    // target_pub_ = nh.advertise<opencv_apps::Line>("/roaddetection/target", 1);
    target_pub_ = nh.advertise<mbzirc_roaddetection::GetDir>("/roaddetection/target", 1);
    
    
    while(ros::ok()){
      ros::spinOnce();
      nh.getParamCached("/mission/mode", mode_);
      loop_rate.sleep();
    }
    return 0;
}
    
